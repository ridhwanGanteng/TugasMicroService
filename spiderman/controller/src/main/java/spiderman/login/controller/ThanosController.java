package spiderman.login.controller;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import spiderman.login.dao.DepositRequest;
import spiderman.login.dao.LoginRequest;
import spiderman.login.response.BaseResponse;
import spiderman.login.service.ThanosService;

@RefreshScope
@RestController
@RequestMapping("/thanos")
public class ThanosController {
	@Autowired
	ThanosService thaSrc;

	// private SimpleDateFormat sdf = new SimpleDateFormat("dd-mm-yy (hh:mm:ss)");

	/*
	 * @RequestMapping(value="/login",method=RequestMethod.POST) public BaseResponse
	 * Login(@RequestBody LoginRequest obj){ BaseResponse respon = new
	 * BaseResponse(); String user = logSrc.login(obj.getNama(),obj.getPassword());
	 * 
	 * respon.setResponseCode("Time : "+sdf.format(new Date()));
	 * //if(user.equalsIgnoreCase("00")){ // respon.setResponseDesc(user); //}
	 * respon.setResponseDesc(user); return respon; }
	 */

	@RequestMapping(value = "/login", method = RequestMethod.POST)
	public String Login(@RequestBody LoginRequest logReq) {
		String run1 = thaSrc.Login(logReq.getNama(), logReq.getPassword());
		return run1;
	}

	@RequestMapping(value = "/ceksaldo", method = RequestMethod.POST)
	public String CekSaldo(@RequestBody DepositRequest depoReq) {
		String run1 = thaSrc.CekSaldo(depoReq.getNomor());
		return run1;
	}

	@RequestMapping(value = "/setor", method = RequestMethod.POST)
	public String Setor(@RequestBody DepositRequest depoReq) {
		String run1 = thaSrc.Setor(depoReq.getNomor(), depoReq.getAmount());
		return run1;
	}

	@RequestMapping(value = "/tarik", method = RequestMethod.POST)
	public String Tarik(@RequestBody DepositRequest depoReq) {
		String run1 = thaSrc.Tarik(depoReq.getNomor(), depoReq.getAmount());
		return run1;
	}

	@RequestMapping(value = "/history", method = RequestMethod.POST)
	public List LsHisto() {
		List run1 = thaSrc.LsHisto();
		return run1;
	}

}
