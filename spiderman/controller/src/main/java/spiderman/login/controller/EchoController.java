package spiderman.login.controller;

import java.io.IOException;

import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import spiderman.login.request.EchoRequest;
import spiderman.login.response.BaseResponse;

@RestController
public class EchoController {
	public static final String TERMINAL_ECHO_PATH = "/webservice/echo**";
	
	@RequestMapping (value=TERMINAL_ECHO_PATH, method = RequestMethod.POST)
	public @ResponseBody
	BaseResponse echo (@RequestBody EchoRequest request) throws IOException {
		BaseResponse response = new BaseResponse();
		if (request.getRequestCode().equals("ECHO")) {
			response.setResponseCode("00");
			response.setResponseDesc("SUCCES");
		} else {
			response.setResponseCode("01");
			response.setResponseDesc("Failed");
		}
		return response;
	}
}
	