package thanos.user.response;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;

@JsonSerialize(include = JsonSerialize.Inclusion.NON_NULL)
public class TransactionResponse {
	private String nomor;
	private String saldoAwal;
	private String saldoAkhir;
	private String error;

	public String getNomor() {
		return nomor;
	}

	public void setNomor(String nomor) {
		this.nomor = nomor;
	}

	public String getSaldoAwal() {
		return saldoAwal;
	}

	public void setSaldoAwal(String saldoAwal) {
		this.saldoAwal = saldoAwal;
	}

	public String getSaldoAkhir() {
		return saldoAkhir;
	}

	public void setSaldoAkhir(String saldoAkhir) {
		this.saldoAkhir = saldoAkhir;
	}

	public String getError() {
		return error;
	}

	public void setError(String error) {
		this.error = error;
	}

}
