package thanos.user.controller;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.POST;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import thanos.user.dao.model.History;
import thanos.user.dao.model.Usera;
import thanos.user.dao.repository.HistoryRepository;
import thanos.user.response.UserResponse;
import thanos.user.service.HistoryService;
import thanos.user.service.UseraService;

@EnableCaching
@RefreshScope
@RestController
// @RequestMapping("/thanos")
@Produces(MediaType.APPLICATION_JSON)
public class UseraController {

	@Autowired
	private UseraService usrSrc;

	@Autowired
	private HistoryService histoSrc;

	@RequestMapping("/cekuser")
	public List<Usera> lsUsr() {
		return usrSrc.findAll();
	}

	// login ngembaliin username dan password
	/*
	 * @POST
	 * 
	 * @RequestMapping("/find")
	 * 
	 * @Consumes(MediaType.APPLICATION_JSON) public Usera findUsr(@RequestBody Usera
	 * usrReq) { return usrSrc.findByNamaAndPassword(usrReq.getNama(),
	 * usrReq.getPassword()); }
	 */

	// TUGAS eureka zuul No 1 login plus history
	@POST
	@RequestMapping("/login")
	@Consumes(MediaType.APPLICATION_JSON)
	public UserResponse Login(@RequestBody Usera usrReq) {
		UserResponse yure = new UserResponse();
		try {
			Usera usr1 = usrSrc.findByNamaAndPassword(usrReq.getNama(), usrReq.getPassword());

			History histo = new History();
			histo.setDescription("Login user: " + usr1.getNama());
			histoSrc.save(histo);

			yure.setNama(usr1.getNama());
			yure.setRole(usr1.getRole());
			return yure;
		} catch (Exception e) {
			yure.setError("Login gagal");
			return yure;
		}

	}

	@POST
	@RequestMapping("/adduser")
	@Consumes(MediaType.APPLICATION_JSON)
	public String AddUsr(@RequestBody Usera usrReq) {
		Usera usrAa = new Usera();
		usrAa.setNama(usrReq.getNama());
		usrAa.setPassword(usrReq.getPassword());
		usrAa.setRole(usrReq.getRole());
		
		try {
			usrSrc.save(usrAa);
			return "Berhasil tambah user";
		}catch(Exception e) {
			return "Gagal tambah user "+e.getMessage();	
		}
		
	}

}
