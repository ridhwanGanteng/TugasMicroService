package thanos.user.request;

public class TransferRequest {
	private String rekAsal;
	private String rekTujuan;
	private String amount;
	
	public String getRekAsal() {
		return rekAsal;
	}
	public void setRekAsal(String rekAsal) {
		this.rekAsal = rekAsal;
	}
	public String getRekTujuan() {
		return rekTujuan;
	}
	public void setRekTujuan(String rekTujuan) {
		this.rekTujuan = rekTujuan;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	

}
