package thanos.user.request;

import java.io.Serializable;

public class DepositRequest implements Serializable{
	private String nomor;
	private String amount;
	
	public String getNomor() {
		return nomor;
	}
	public void setNomor(String nomor) {
		this.nomor = nomor;
	}
	public String getAmount() {
		return amount;
	}
	public void setAmount(String amount) {
		this.amount = amount;
	}
	

}
