package thanos.user.dao.model;

import java.io.Serializable;
import java.time.LocalDateTime;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.hibernate.annotations.CreationTimestamp;

@Entity
@Table(name = "tb_history")
public class History implements Serializable{

	private static final long serialVersionUID = -2163490103307345064L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "id", nullable = false, unique = true, updatable = false)
	private Integer id;

	//@Temporal(TemporalType.TIMESTAMP)
	@CreationTimestamp
	@Column(name = "waktu", nullable = false)
	private LocalDateTime waktu;

	@Column(name = "description", nullable = false)
	private String description;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public LocalDateTime getWaktu() {
		return waktu;
	}

	public void setWaktu(LocalDateTime waktu) {
		this.waktu = waktu;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
