package thanos.user.dao.repository;

import org.springframework.data.repository.CrudRepository;

import thanos.user.dao.model.Usera;

public interface UseraRepository extends CrudRepository<Usera, Long>{
	
	public abstract Usera findByNamaAndPassword(String nama, String password);

}
